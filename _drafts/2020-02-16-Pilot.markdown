---
layout: podcast
categories: podcast # podcast
title: "S1E0: Pilot"
author: "Science Activities Club"
season: 1
episode: 0
episodeType: full # full | trailer | bonus
explicit: false # true | false
audio: ../../podcasts/generic-2.mp3
date: 2020-02-16 00:28:00
length: 212 # in seconds
---
This post is a representation of any reference code that you might need, for setting up a new post. This might be taken as a template of sorts.
Some code for code:

```javascript
const express = require('express')
const app = express()

app.get('/', function (req, res) {
  res.send('Hello World')
})

app.listen(3000)
```
Some stuff for behaviour of links:

Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can ask them on [Jekyll Talk][jekyll-talk].

[jekyll-docs]: https://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
